package team.susanin.susanin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by AMamonov on 5/14/2018.
 */

public class StringValidator {


    public StringValidator() {
    }

    public boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
//        return true;
    }


    public boolean isPasswordValid(String password) {
//                   ^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$
//                 ^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$

//        String expression = "^(?=.*[A-Za-z])(?=.*\\\\d)(?=.*[$@$!%*#?&])[A-Za-z\\\\d$@$!%*#?&]{8,}$";
//        CharSequence inputStr = password;
//
//        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(inputStr);
//
//        return matcher.matches();
        if (password.length() >= 8) {
            return  true;
        }else{
            return false;
        }
    }

    public boolean isDateValid(String format, CharSequence value) {
//        Date date = null;
//        try {
//            SimpleDateFormat sdf = new SimpleDateFormat(format);
//            date = sdf.parse(value.toString());
//            if (!value.equals(sdf.format(date))) {
//                date = null;
//            }
//        } catch (ParseException ex) {
//            ex.printStackTrace();
//        }
//        return date != null;
        return true;
    }

    public boolean isFieldNotEmpty(String sequence){
        if (sequence.length() > 0){
            return  true;
        }
        else {
            return false;
        }
//        return true;
    }
}
