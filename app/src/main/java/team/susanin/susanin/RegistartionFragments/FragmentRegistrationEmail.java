package team.susanin.susanin.RegistartionFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import team.susanin.susanin.R;
import team.susanin.susanin.StringValidator;

public class FragmentRegistrationEmail extends Fragment {
    View view;
    EditText emailText;
    EditText passwordText;
    EditText passwordRepeatText;
    // Exceptions of view
    TextView emailEmptyException;
    TextView emailNotValidException;
    TextView passwordEmptyException;
    TextView passwordNotValidException;
    TextView passwordsNotSameException;

    public FragmentRegistrationEmail() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.registration_email_fragment, container, false);
        emailText = view.findViewById(R.id.emailText);
        passwordText = view.findViewById(R.id.passwordText);
        passwordRepeatText = view.findViewById(R.id.passwordRepeatText);
        // Exceptions of view
         emailEmptyException = view.findViewById(R.id.exceptionEmailEmptyText);
         emailNotValidException = view.findViewById(R.id.exceptionEmailNotVaidText);
         passwordEmptyException = view.findViewById(R.id.exсeptionPasswordEmptyText);
         passwordNotValidException = view.findViewById(R.id.exсeptionPasswordNotValidText);
         passwordsNotSameException = view.findViewById(R.id.exceptionPasswordsNotSameText);
        return view;
    }

    public String getEmailText() {
        return emailText.getText().toString();
    }

    public String getPasswordText() {
        return passwordText.getText().toString();
    }

    public boolean checkFields(StringValidator stringValidator){
        emailEmptyException.setVisibility(View.INVISIBLE);
        emailNotValidException.setVisibility(View.INVISIBLE);
        passwordEmptyException.setVisibility(View.INVISIBLE);
        passwordNotValidException.setVisibility(View.INVISIBLE);
        passwordsNotSameException.setVisibility(View.INVISIBLE);
        if (stringValidator.isFieldNotEmpty(emailText.getText().toString())){
            if(stringValidator.isEmailValid(emailText.getText().toString())) {
                if (stringValidator.isFieldNotEmpty(passwordText.getText().toString())) {
                    if (stringValidator.isPasswordValid(passwordText.getText().toString())) {
                        if (passwordText.getText().toString().equals(passwordRepeatText.getText().toString())) {
                            return true;
                        }
                        else {
                            passwordsNotSameException.setVisibility(View.VISIBLE);
                            return false;
                        }
                    }
                    else {
                        passwordNotValidException.setVisibility(View.VISIBLE);
                        return false;
                    }
                }
                else{
                    passwordEmptyException.setVisibility(View.VISIBLE);
                    return false;
                }
            }
            else {
                emailNotValidException.setVisibility(View.VISIBLE);
                return false;
            }
        }
        else {
            emailEmptyException.setVisibility(View.VISIBLE);
            return false;
        }
    }

    public  void goNext(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }

    public void goBack(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }

}
