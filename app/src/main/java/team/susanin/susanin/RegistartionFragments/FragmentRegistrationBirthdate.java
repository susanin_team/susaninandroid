package team.susanin.susanin.RegistartionFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import team.susanin.susanin.CloudListItem;
import team.susanin.susanin.CloudListRecyclerViewAdapter;
import team.susanin.susanin.CloudListViewAdapter;
import team.susanin.susanin.Constants;
import team.susanin.susanin.R;
import team.susanin.susanin.Requests.GetRequests;
import team.susanin.susanin.StringValidator;
import team.susanin.susanin.WrappedDialog;

/**
 * Created by AMamonov on 5/10/2018.
 */

public class FragmentRegistrationBirthdate extends Fragment{

    View view;
    TextView aboutText;
    TextView monthText;
    TextView dateText;
    TextView yearText;
    //exceptions of birthdateFragment
    TextView dateNotValidException;
    public ArrayList<CloudListItem> langList;
    public ArrayList<CloudListItem> cityList;
    private Button langSelectButton;
    private RecyclerView mLangRecycler, mCityRecycler;
    private Button citySelectButton;
    public JSONArray citiesArray, languagesArray;
    public FragmentRegistrationBirthdate() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.registration_birthdate_fragment, container, false);
        aboutText = view.findViewById(R.id.aboutMeText);
        monthText = view.findViewById(R.id.monthOfBirthText);
        dateText = view.findViewById(R.id.dateOfBirthText);
        yearText = view.findViewById(R.id.yearOfBirthText);
        dateNotValidException = view.findViewById(R.id.exceptionDateNotValid);


        final class CitiesCallback implements GetRequests.Callback<JSONArray, VolleyError> {
            @Override
            public void onEventCompleted(JSONArray response) {
                System.out.println(response);
                citiesArray = response;
                try {
                    for(int i=0;i < citiesArray.length();i++){
                        cityList.add(new CloudListItem(citiesArray.getJSONObject(i).getString("name"), false));
                    }
                } catch (JSONException e) {
                    // it the json is not array it throws exception so you can catch second case here
                    e.printStackTrace();
                }
            }
            @Override
            public void onEventFailed(VolleyError error) {
                System.out.println(error);
            }
        }

        final class LanguagesCallback implements GetRequests.Callback<JSONArray, VolleyError> {
            @Override
            public void onEventCompleted(JSONArray response) {
                System.out.println(response);
                languagesArray = response;
                try {
                    for(int i=0;i < languagesArray.length();i++){
                        langList.add(new CloudListItem(languagesArray.getJSONObject(i).getString("name"), false));
                    }
                } catch (JSONException e) {
                    // it the json is not array it throws exception so you can catch second case here
                    e.printStackTrace();
                }
            }
            @Override
            public void onEventFailed(VolleyError error) {
                System.out.println(error);
            }
        }


        GetRequests  getLanguages = new GetRequests();
        GetRequests  getCities = new GetRequests();

        langList = new ArrayList<>();
        final LanguagesCallback  languagesCallback = new LanguagesCallback();
        getLanguages.registerCallBack(languagesCallback);
        getLanguages.getArray(Constants.LANGUAGES, getContext());

        cityList = new ArrayList<>();
        final CitiesCallback citiesCallback = new CitiesCallback();
        getCities.registerCallBack(citiesCallback);
        getCities.getArray(Constants.CITIES, getContext());



//        JSONArray languagesJsonArray = getRequests.returnArray(Constants.LANGUAGES, getContext());
//        try {
//            for(int i=0;i < languagesJsonArray.length();i++){
//                langList.add(new CloudListItem(languagesJsonArray.getJSONObject(i).getString("name"), false));
//            }
//        } catch (JSONException e) {
//            // it the json is not array it throws exception so you can catch second case here
//            e.printStackTrace();
//        }
        // Test list
//        langList.add(new CloudListItem("Russian", false));
//        langList.add(new CloudListItem("English", false));
//        langList.add(new CloudListItem("Chinese", false));
//        langList.add(new CloudListItem("German", false));
//        langList.add(new CloudListItem("Polish", false));
//        langList.add(new CloudListItem("Hebrew", false));
//        langList.add(new CloudListItem("Latin", false));
//        langList.add(new CloudListItem("Sweden", false));
//        langList.add(new CloudListItem("Norwegian", false));
//        langList.add(new CloudListItem("Tatar", false));
        langSelectButton = view.findViewById(R.id.languageSelectionButton);
        mLangRecycler = view.findViewById(R.id.languagesRecycler);

        final CloudListViewAdapter langAdapter = new CloudListViewAdapter(getActivity(), langList);
        final CloudListRecyclerViewAdapter langRecyclerAdapter = new CloudListRecyclerViewAdapter(langList, langSelectButton);
        langSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View langView = getLayoutInflater().inflate(R.layout.choose_languages, null);
                ListView langListView;
                // Language selection button
                Button selectButton;
                langListView = langView.findViewById(R.id.languagesList);
                selectButton = langView.findViewById(R.id.selectionButton);
                langListView.setAdapter(langAdapter);
                final WrappedDialog langDialog = new WrappedDialog();
                selectButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        langRecyclerAdapter.buildActualList();
                        // Hide select button
                        if(langRecyclerAdapter.getItemCount() > 0)
                            langSelectButton.setVisibility(View.GONE);
                        // Refresh view
                        langRecyclerAdapter.notifyDataSetChanged();
                        langDialog.dismiss();
                    }
                });
                langDialog.setView(langView);
                langDialog.show(getActivity().getFragmentManager(), null);
            }
        });

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getActivity());
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        mLangRecycler.setLayoutManager(layoutManager);
        mLangRecycler.setAdapter(langRecyclerAdapter);
        // Make selection button visible if no languages are chosen
        if(langList.size() == 0) {
            langSelectButton.setVisibility(View.VISIBLE);
        }

//        JSONArray citiesJsonArray;

//        cityList.add(new CloudListItem("Moscow", false));
//        cityList.add(new CloudListItem("Nizhny Novgorod", false));

        citySelectButton = view.findViewById(R.id.citySelectionButton);
        mCityRecycler = view.findViewById(R.id.citiesRecycler);

        final CloudListViewAdapter cityAdapter = new CloudListViewAdapter(getActivity(), cityList);
        final CloudListRecyclerViewAdapter cityRecyclerAdapter = new CloudListRecyclerViewAdapter(cityList, citySelectButton);
        citySelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View cityView = getLayoutInflater().inflate(R.layout.choose_cities, null);
                ListView cityListView;
                // City selection button
                Button citySelectioButton;
                cityListView = cityView.findViewById(R.id.citiesList);
                citySelectioButton = cityView.findViewById(R.id.selectionButton);
                cityListView.setAdapter(cityAdapter);
                final WrappedDialog cityDialog = new WrappedDialog();
                citySelectioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cityRecyclerAdapter.buildActualList();
                        // Hide select button
                        if(cityRecyclerAdapter.getItemCount() > 0)
                            citySelectButton.setVisibility(View.GONE);
                        // Refresh view
                        cityRecyclerAdapter.notifyDataSetChanged();
                        cityDialog.dismiss();
                    }
                });
                cityDialog.setView(cityView);
                cityDialog.show(getActivity().getFragmentManager(), null);
            }
        });

        FlexboxLayoutManager cityLayoutManager = new FlexboxLayoutManager(getActivity());
        cityLayoutManager.setFlexWrap(FlexWrap.WRAP);
        mCityRecycler.setLayoutManager(cityLayoutManager);
        mCityRecycler.setAdapter(cityRecyclerAdapter);
        // Make selection button visible if no languages are chosen
        if(cityList.size() == 0) {
            citySelectButton.setVisibility(View.VISIBLE);
        }


        return view;
    }

    public String getAboutText(){
        return aboutText.getText().toString();
    }

    public String getBirthDateText(){
        return  yearText.getText().toString() + "-" + monthText.getText().toString() + "-" + dateText.getText().toString();
    }

    public boolean checkFields(StringValidator stringValidator){

        dateNotValidException.setVisibility(View.INVISIBLE);
        String birthdate = yearText.getText().toString() + "-" + monthText.getText().toString() + "-" + dateText.getText().toString();
        if (stringValidator.isDateValid("yyyy-MM-dd", birthdate)) {
            System.out.println("Trying to register");
//                                register(email, password, name, secondName, birthdate, about);
            return true;
        }
        else {
            dateNotValidException.setVisibility(View.VISIBLE);
            return false;
        }
    }

    public  void goNext(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }

    public void goBack(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }

    public ArrayList<String> getCitiesArray() {
        ArrayList arrayList1 = new ArrayList();
        for(int i=0;i < cityList.size();i++){
            for (int j=0; j < citiesArray.length(); j++){
                try {
                    if (cityList.get(i).getLangName() == citiesArray.getJSONObject(j).getString("name") && cityList.get(i).isChecked())
                        arrayList1.add(citiesArray.getJSONObject(j).getString("url"));
                }
                catch (JSONException e) {

                }

            }
        }
        return arrayList1;
    }

    public ArrayList<String> getLanguagesArray() {
        ArrayList arrayList1 = new ArrayList();
        for(int i=0;i < langList.size();i++){
            for (int j=0; j < languagesArray.length(); j++){
                try {
                    if (langList.get(i).getLangName() == languagesArray.getJSONObject(j).getString("name") && langList.get(i).isChecked())
                        arrayList1.add(languagesArray.getJSONObject(j).getString("url"));
                }
                catch (JSONException e) {

                }

            }
        }
        return arrayList1;
    };
}
