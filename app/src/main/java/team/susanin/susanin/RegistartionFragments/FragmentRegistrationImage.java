package team.susanin.susanin.RegistartionFragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import team.susanin.susanin.BoolListener;
import team.susanin.susanin.Constants;
import team.susanin.susanin.R;
import team.susanin.susanin.Requests.PostRequest;
import team.susanin.susanin.StringValidator;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by AMamonov on 5/10/2018.
 */

public class FragmentRegistrationImage extends Fragment{

    View view;
    private Button uploadImageBtn;
    private ImageView imageView;

    private static final int STORAGE_PERMISSION_CODE = 1234;
    private static final int PICK_IMAGE_REQUEST = 22;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private Uri filepath;
    private Bitmap bitmap;
    private String imageToString;
    public String imgId = null;
//    public Boolean isSent = false;

    public BoolListener isSent = new BoolListener();

    final class CallbackClass implements PostRequest.Callback<JSONObject, VolleyError> {
        @Override
        public void onEventCompleted(JSONObject responce) {
            try {
                Log.d(TAG, responce.toString());
                imgId = Constants.IMAGES_URL +  responce.getInt("imgId");
                Log.d(TAG, imgId);
                isSent.setBoo(true);
            }
            catch (JSONException  e) {
                Log.e(TAG, "onEventCompleted: ", e );
                imgId = null;
                isSent.setBoo(true);
            }
        }
        @Override
        public void onEventFailed(VolleyError error) {
            Log.e(TAG, "upload_failed", error);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        requestStoragePremisson();
        requestCameraPremisson();
        view = inflater.inflate(R.layout.registration_image_fragment, container, false);
        uploadImageBtn = view.findViewById(R.id.chooseFromGalleryBtn);
        imageView = view.findViewById(R.id.imageView);
        uploadImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        view.findViewById(R.id.imageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                }
                else
                {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });
        return view;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
    }

    private void requestStoragePremisson(){
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED )
            return;

        ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    private void requestCameraPremisson(){
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            return;

        ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
    }


    private void showFileChooser() {
        Intent intent  = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && data != null) {
            filepath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getActivity().getContentResolver(), filepath);
                imageView.setImageBitmap(bitmap);
                imageToString = imageToString(bitmap);
//                imgId = uploadImage();
            }
            catch (IOException e) {

            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Matrix matrix = new Matrix();
            bitmap =  (Bitmap) data.getExtras().get("data");
            if (bitmap.getWidth() > bitmap.getHeight()){
                matrix.postRotate(90);
            }
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            imageView.setImageBitmap(bitmap);
            imageToString = imageToString(bitmap);
        }
    }

    public  String returnImage(){
        return imgId;
    }

    public JSONObject createImageUploadJson(String imageToString){
        final JSONObject root = new JSONObject();
        try {
            root.put("image", imageToString);
            return root;
        } catch (JSONException ej){
            Log.d("JWP", "Cant' format Json");
        }
        return null;
    }

    public boolean checkFields(StringValidator stringValidator){
        return true;
    }

    public void uploadImage(){
        PostRequest postRequest = new PostRequest();
        final CallbackClass callbackClass = new CallbackClass();
        postRequest.registerCallBack(callbackClass);
        JSONObject imageUploadJson = createImageUploadJson(imageToString);
        System.out.println(imageUploadJson.length());
        if (imageUploadJson.length() != 0) {
            System.out.println(imageUploadJson);
            postRequest.post(Constants.UPLOAD_IMAGE, imageUploadJson, this.getContext());
        }
//        return imgId;
    }

    public String imageToString(Bitmap bitmap) {
        if (bitmap == null){
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    public void goNext(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }

    public void goBack(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }

}
