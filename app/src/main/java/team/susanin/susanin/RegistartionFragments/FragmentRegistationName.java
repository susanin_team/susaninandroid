package team.susanin.susanin.RegistartionFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import team.susanin.susanin.R;
import team.susanin.susanin.StringValidator;

public class FragmentRegistationName extends Fragment {
    View view;
    EditText nameText;
    EditText secondNameText;
    // Exceptions of nameFragment
    TextView nameException;
    TextView secondNameException;

    public FragmentRegistationName() {
        // Empty
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.registration_name_fragment, container, false);
        nameText = view.findViewById(R.id.nameText);
        secondNameText = view.findViewById(R.id.secondNameText);
        // Exceptions of view
        nameException = view.findViewById(R.id.exceptionNameText);
        secondNameException = view.findViewById(R.id.exceptionSecondNameText);
        return view;
    }
    public String getNameText() {
        return nameText.getText().toString();
    }

    public String getSecondNameText() {
        return secondNameText.getText().toString();
    }

    public boolean checkFields(StringValidator stringValidator){
        nameException.setVisibility(View.INVISIBLE);
        secondNameException.setVisibility(View.INVISIBLE);
        if(stringValidator.isFieldNotEmpty(nameText.getText().toString())){
            if (stringValidator.isFieldNotEmpty(secondNameText.getText().toString())){
                return true;
            }
            else {
                secondNameException.setVisibility(View.VISIBLE);
                return false;
            }
        } else {
            nameException.setVisibility(View.VISIBLE);
            return false;
        }
    }
    public  void goNext(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }
    public void goBack(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }
}
