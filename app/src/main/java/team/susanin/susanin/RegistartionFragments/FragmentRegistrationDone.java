package team.susanin.susanin.RegistartionFragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import team.susanin.susanin.R;

public class FragmentRegistrationDone extends Fragment {
    View view;

    public FragmentRegistrationDone() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.registration_done_fragment, container, false);
        return view;
    }

    public void goBack(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
    }

    public  void goNext(ViewPager viewPager){
        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
    }
}
