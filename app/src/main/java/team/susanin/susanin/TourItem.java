package team.susanin.susanin;

public class TourItem {
    private String title;
    private float rating;
    private int visitors;
    private Boolean isAdult;

    public TourItem(String tit, float rat, int vis, Boolean isAdlt) {
        title = tit;
        rating = rat;
        visitors = vis;
        isAdult = isAdlt;
    }
    public String getTitle() {
        return title;
    }
    public float getRating() {
       return rating;
    }
    public int getVisitors() {
        return visitors;
    }
    public Boolean isAdult() {
        return isAdult;
    }
}
