package team.susanin.susanin;

public class SplashProgressDrawer implements Runnable {
    private double progress;
    private SplashActivity spl;
    SplashProgressDrawer(SplashActivity splashAct) {
        spl = splashAct;
    }
    public void setProgress(double p) {
        progress = p;
    }
    @Override
    public void run() {
        spl.drawLoader((float) progress);
    }
}
