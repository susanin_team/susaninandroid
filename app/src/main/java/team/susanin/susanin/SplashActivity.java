package team.susanin.susanin;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

public class SplashActivity extends Activity {
    private Bitmap bmLoader, bmFilled;
    private ImageView loader;
    private RectF oval;
    private Paint arcPaint;

    protected void drawLoader(float angle) {
        bmLoader = (BitmapFactory.decodeResource(getResources(), R.drawable.splash_loading_empty)).copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bmLoader);
        // Lock layer
        canvas.saveLayer(oval, null, Canvas.HAS_ALPHA_LAYER_SAVE_FLAG);
        canvas.drawBitmap(bmFilled, new Matrix(), null);
        canvas.drawArc(oval, angle - 90, 360 - angle, true, arcPaint);
        // Unlock
        canvas.restore();
        loader.setImageDrawable(new BitmapDrawable(getResources(), bmLoader));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set activity to fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Set content
        setContentView(R.layout.activity_splash);
        loader = findViewById(R.id.splashLoading);
        // Filled
        bmFilled = BitmapFactory.decodeResource(getResources(), R.drawable.splash_loading_full);
        oval = new RectF(-2, -2, bmFilled.getWidth() + 2, bmFilled.getHeight() + 2);
        arcPaint = new Paint();
        arcPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        // Prepare information in separate thread
        Thread loadAppTh = new Thread(new Runnable() {
            @Override
            public void run() {
                float readyPercent = 0;
                SplashProgressDrawer progressDrawer = new SplashProgressDrawer(SplashActivity.this);
                while (readyPercent <= 359 ) {
                    runOnUiThread(progressDrawer);
                    readyPercent += 1;
                    progressDrawer.setProgress(readyPercent);
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        loadAppTh.start();
    }
}
