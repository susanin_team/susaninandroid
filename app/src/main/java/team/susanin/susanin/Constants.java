package team.susanin.susanin;

/**
 * Created by AMamonov2 on 10/22/2018.
 */

public class Constants {
    public static String HOST = "http://92.242.70.164";
//    public static String HOST = "http://10.0.2.2:90";
    public static String REGISTER_REQUEST_URL =  HOST + "/api/v1/guestregistration/";
    public static String LOGIN_REQUEST_URL = HOST + "/rest-auth/login/";
    public static String CITIES =  HOST + "/api/v1/city/";
    public static String LANGUAGES =  HOST + "/api/v1/language/";
    public static String UPLOAD_IMAGE = HOST + "/upload_image/";
    public static String IMAGES_URL = HOST + "/api/v1/routeimage/";
    // public static String IMAGES_URL = "http://127.0.0.1:90/api/v1/routeimage/";
}
