package team.susanin.susanin;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileCommentView extends LinearLayout {
    private String userName;
    private String message;
    private TextView userNameField;
    private TextView messageField;

    public ProfileCommentView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public ProfileCommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public ProfileCommentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        init(context, attrs, defStyleAttr);
    }
    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ProfileCommentView, defStyleAttr, 0);
        try{
            userName = array.getString(R.styleable.ProfileCommentView_userName);
            message = array.getString(R.styleable.ProfileCommentView_message);
            // Inflating XML layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutInflater.inflate(R.layout.profile_comment, this, true);
            userNameField = findViewById(R.id.userName);
            messageField = findViewById(R.id.comment);
            userNameField.setText(userName);
            messageField.setText(message);
        } catch(Exception e) {
            // Catching exception
        } finally {
            array.recycle();
        }
    }
    public void setProfilePicture(Bitmap picBitmap) {
        CircleImageView imView;
        imView = findViewById(R.id.userPic);
        // Setting image bitmap
        imView.setImageBitmap(picBitmap);
    }
}
