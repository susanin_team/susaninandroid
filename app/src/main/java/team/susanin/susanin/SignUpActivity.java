package team.susanin.susanin;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import team.susanin.susanin.RegistartionFragments.FragmentRegistationName;
import team.susanin.susanin.RegistartionFragments.FragmentRegistrationBirthdate;
import team.susanin.susanin.RegistartionFragments.FragmentRegistrationDone;
import team.susanin.susanin.RegistartionFragments.FragmentRegistrationEmail;
import team.susanin.susanin.RegistartionFragments.FragmentRegistrationImage;
import team.susanin.susanin.Requests.PostRequest;

public class SignUpActivity extends AppCompatActivity  {
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private MenuItem item;
    private  Button nextBtn;
    private Toolbar toolbar;
    private TextView hint;
    String email, password, birthdate, name, secondName, about;
    String image = null;
    ArrayList<String> languages = new ArrayList<>();
    ArrayList<String> cities = new ArrayList<>();
    // Creating Fragments objects
    final FragmentRegistrationEmail fragmentRegistrationEmail = new FragmentRegistrationEmail();
    final FragmentRegistationName fragmentRegistationName = new FragmentRegistationName();
    final FragmentRegistrationBirthdate fragmentRegistrationBirthdate = new FragmentRegistrationBirthdate();
    final FragmentRegistrationImage fragmentRegistrationImage = new FragmentRegistrationImage();
    final FragmentRegistrationDone fragmentRegistrationDone = new FragmentRegistrationDone();


    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_sign_up);
        dotsLayout = findViewById(R.id.dotsLayout);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        // Toolbar init
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (java.lang.NullPointerException e) {
            Log.e("getSupportActionBar", "onCreate: ", e);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //put fragments in ViewPageAdapter
        ViewPageAdapter adapter = new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragment(fragmentRegistrationEmail, "Email");
        adapter.addFragment(fragmentRegistationName, "Name");
        adapter.addFragment(fragmentRegistrationImage, "Image");
        adapter.addFragment(fragmentRegistrationBirthdate, "About");
        adapter.addFragment(fragmentRegistrationDone, "Registration Done");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        final StringValidator stringValidator = new StringValidator();

        final class CallbackClass implements PostRequest.Callback<JSONObject, VolleyError> {
            @Override
            public void onEventCompleted(JSONObject responce) {
                fragmentRegistrationBirthdate.goNext(viewPager);
                nextBtn.setText(getString(R.string.startTrip));
            }
            @Override
            public void onEventFailed(VolleyError error) {
                TextView serverException = findViewById(R.id.exceptionServer);
                if(error.networkResponse != null && error.networkResponse.data != null)
                    serverException.setText("Server Error");
                serverException.setVisibility(View.VISIBLE);
            }
        }



        final CallbackClass callbackClass = new CallbackClass();

        hint = findViewById(R.id.hintTextView);
        // Initialize next button and implements scroll in custom viewpager
        nextBtn = findViewById(R.id.nextBtn);
        hint.setText(R.string.inputEmailAndPassword);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (viewPager.getCurrentItem()){
                    case 0:
                        if (fragmentRegistrationEmail.checkFields(stringValidator)){
                            email = fragmentRegistrationEmail.getEmailText();
                            password = fragmentRegistrationEmail.getPasswordText();
                            fragmentRegistrationEmail.goNext(viewPager);
                            hint.setText(R.string.inputYourFirstAndSecondName);
                        }
                        break;
                    case 1:
                        if (fragmentRegistationName.checkFields(stringValidator)){
                            name = fragmentRegistationName.getNameText();
                            secondName = fragmentRegistationName.getSecondNameText();
                            fragmentRegistationName.goNext(viewPager);
                            hint.setText(R.string.addYourImage);
                        }
                        break;
                    case 2:
                        if (fragmentRegistrationImage.checkFields(stringValidator)){
                            fragmentRegistrationImage.uploadImage();
                            nextBtn.setText(getString(R.string.uploadingUmage));
                            fragmentRegistrationImage.isSent.setListener(new BoolListener.ChangeListener() {
                                @Override
                                public void onChange() {
                                    image = fragmentRegistrationImage.returnImage();
                                    fragmentRegistrationImage.goNext(viewPager);
                                    nextBtn.setText(getString(R.string.signup));
                                    hint.setText(R.string.tellAboutYourself);
                                }
                            });
                        }
                        break;
                    case 3:
                        try {
                            fragmentRegistrationBirthdate.getActivity().findViewById(R.id.exceptionServer).setVisibility(View.INVISIBLE);
                        } catch (java.lang.NullPointerException e) {
                            Log.e("findViewById", "onClick: ",e);
                        }
                        if (fragmentRegistrationBirthdate.checkFields(stringValidator)){
                            about = fragmentRegistrationBirthdate.getAboutText();
                            birthdate = fragmentRegistrationBirthdate.getBirthDateText();
//                            languages.add("http://pollyndrome.com/api/v1/languages/1");
//                            cities.add("http://pollyndrome.com/api/v1/cities/1");
                            cities = fragmentRegistrationBirthdate.getCitiesArray();
                            languages = fragmentRegistrationBirthdate.getLanguagesArray();
//                            hint.setText(R.string.registrationDone);
                            System.out.println(cities);
                            System.out.println(languages);
                            PostRequest postRequest = new PostRequest();
                            postRequest.registerCallBack(callbackClass);
                            System.out.println(registrationFormatDataAsJson(name, secondName, email, image, password, birthdate, about, languages, cities));
                            postRequest.post(Constants.REGISTER_REQUEST_URL, registrationFormatDataAsJson(name, secondName, email, image, password, birthdate, about, languages, cities), getApplicationContext());
                        }
                        break;
                    case 4:
                        finish();
                        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                        break;
                }
            }
        });
        addDotsIndicator(0);
        viewPager.addOnPageChangeListener(viewListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
                switch (viewPager.getCurrentItem()){
                    case 0:
                        finish();
                        break;
                    case 1:
                        fragmentRegistationName.goBack(viewPager);
                        hint.setText(R.string.inputEmailAndPassword);
                        break;
                    case 2:
                        fragmentRegistrationImage.goBack(viewPager);
                        hint.setText(R.string.inputYourFirstAndSecondName);
                        break;
                    case 3:
                        nextBtn.setText(getString(R.string.next));
                        fragmentRegistrationBirthdate.goBack(viewPager);
                        hint.setText(R.string.addYourImage);
                        break;
                    case 4:
                        hint.setText(R.string.tellAboutYourself);
                        fragmentRegistrationDone.goBack(viewPager);
                }
            }
        return super.onOptionsItemSelected(item);
    }

    public void addDotsIndicator(int position){
        dots = new TextView[5];
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++){
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.lightGray));

            dotsLayout.addView(dots[i]);
        }
        if (dots.length > 0){
            dots[position].setTextColor(getResources().getColor(R.color.backGroundColor));
            if (position > 0 && position <= dots.length-2){
                for (int i = 0; i < position; i++) {
                    dots[i].setTextColor(getResources().getColor(R.color.backGroundColor));
                }
            }
            if (position == dots.length-1){
                for (TextView dot: dots){
                    dot.setTextColor(getResources().getColor(R.color.forestGreen));
                }
            }
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener(){
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDotsIndicator(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public JSONObject registrationFormatDataAsJson(String name, String secondName, String email, String image, String password, String birthDate, String about, ArrayList<String> languages, ArrayList<String> cities){
        final JSONObject root = new JSONObject();
        try {
            root.put("first_name", name);
            root.put("phone_number", "");
            root.put("last_name", secondName);
            root.put("phone_number", "+79601840934");
            root.put("email", email);
            root.put("password", password);
            root.put("description", about);

            if (image != null) {
                JSONArray imageJson = new JSONArray();
                imageJson.put(image);
                root.put("images", imageJson);
            }

            JSONArray citiesJson = new JSONArray();
            if (cities != null){
                for (int i = 0; i< cities.size(); i++){
                    citiesJson.put(cities.get(i).toString());
                }
                root.put("city", citiesJson);
            }

            JSONArray languagesJson = new JSONArray();

            if (cities != null){
                for (int i = 0; i< languages.size(); i++){
                    languagesJson.put(languages.get(i).toString());
                }
                root.put("language", languagesJson);
            }

            return root;
        } catch (JSONException ej){
            Log.d("JWP", "Cant' format Json");
        }
        return null;
    }


    public void changeScreen(View v) {
        startActivity(new Intent(SignUpActivity.this, MainScreenActivity.class));
    }
}
