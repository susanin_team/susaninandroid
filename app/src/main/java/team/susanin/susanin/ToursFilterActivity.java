package team.susanin.susanin;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import java.util.ArrayList;

public class ToursFilterActivity extends AppCompatActivity {
    private boolean filtersExpanded;
    private ImageView toggleFiltersButton;
    private ArrayList<TourItem> toursList;
    private RecyclerView toursListView;
    private LinearLayout filtersTitleContainer;
    private ImageView filterButton;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private ArrayList<CloudListItem> langList;
    // Price bars
    private SeekBar fromPriceBar;
    private SeekBar toPriceBar;
    // Price fields
    private EditText fromPriceField;
    private EditText toPriceField;
    // Guide prices
    private int priceMax = 10000;
    private int fromPriceValue = 0;
    private int toPriceValue = 0;
    private Button langSelectButton;
    private RecyclerView mLangRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tours_filter);
        // Test guide list init
        toursList = new ArrayList<>();
        toursList.add(new TourItem("Tour title", 4.5f, 10001, false));
        toursList.add(new TourItem("Tour title", 4.1f, 1000234, true));
        toursList.add(new TourItem("Tour title", 4.5f, 1, false));
        toursList.add(new TourItem("Tour title", 4.5f, 0, false));
        toursList.add(new TourItem("Tour title", 4.5f, 124345, false));
        toursList.add(new TourItem("Tour title", 4.5f, 23124, false));
        toursList.add(new TourItem("Tour title", 4.5f, 23, true));
        toursList.add(new TourItem("Tour title", 4.5f, 4, false));
        langList = new ArrayList<>();
        // Test list
        langList.add(new CloudListItem("Sports", false));
        langList.add(new CloudListItem("Restaurants", false));
        langList.add(new CloudListItem("Night clubs", false));
        langList.add(new CloudListItem("Museums", true));
        langList.add(new CloudListItem("For kids", false));
        langSelectButton = findViewById(R.id.languageSelectionButton);
        toggleFiltersButton = findViewById(R.id.toggleFilters);
        toursListView = findViewById(R.id.searchResultsList);
        mLangRecycler = findViewById(R.id.languagesRecycler);
        toursListView.setAdapter(new ToursRecyclerViewAdapter(this, toursList));
        toursListView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        filtersTitleContainer = findViewById(R.id.filtersToggleTitle);
        // Setting search options to be expandable
        filtersTitleContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout filtersLayout;
                filtersLayout = findViewById(R.id.searchParams);
                if(filtersExpanded) {
                    collapse(filtersLayout);
                    filtersExpanded = false;
                } else {
                    expand(filtersLayout);
                    filtersExpanded = true;
                }
            }
        });
        // Toolbar init
        mDrawerLayout = findViewById(R.id.drawerContainer);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        filterButton = findViewById(R.id.toggleFilters);
        filtersExpanded = true;
        // Prices fields retrieving
        fromPriceBar = findViewById(R.id.fromSeekBar);
        toPriceBar = findViewById(R.id.toSeekBar);
        fromPriceField = findViewById(R.id.fromInput);
        toPriceField = findViewById(R.id.toInput);
        // Setting default values
        fromPriceBar.setMax(priceMax);
        toPriceBar.setMax(priceMax);
        fromPriceBar.setProgress(fromPriceValue);
        toPriceBar.setProgress(toPriceValue);
        fromPriceField.setText("" + fromPriceValue, TextView.BufferType.EDITABLE);
        toPriceField.setText("" + toPriceValue, TextView.BufferType.EDITABLE);

        fromPriceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(i > toPriceValue) {
                    i = toPriceValue;
                    seekBar.setProgress(i);
                }
                fromPriceValue = i;
                fromPriceField.setTag("");
                fromPriceField.setText("" + fromPriceValue, TextView.BufferType.EDITABLE);
                fromPriceField.setTag(null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        toPriceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(i < fromPriceValue) {
                    i = fromPriceValue;
                    seekBar.setProgress(i);
                }
                toPriceValue = i;
                toPriceField.setTag("");
                toPriceField.setText("" + toPriceValue, TextView.BufferType.EDITABLE);
                toPriceField.setTag(null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        fromPriceField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(fromPriceField.getTag() == null) {
                    int val = 0;
                    if (fromPriceField.getText().toString().compareTo("") != 0)
                        val = Integer.parseInt(fromPriceField.getText().toString());
                    if (val > toPriceValue) {
                        val = toPriceValue;
                        fromPriceField.setTag("");
                        fromPriceField.setText("" + val, TextView.BufferType.EDITABLE);
                        fromPriceField.setTag(null);
                    }
                    if (val > priceMax) {
                        val = priceMax;
                        fromPriceField.setTag("");
                        fromPriceField.setText("" + val, TextView.BufferType.EDITABLE);
                        fromPriceField.setTag(null);
                    }
                    fromPriceValue = val;
                    fromPriceBar.setProgress(val);
                }
                // Set cursor to the end of the field
                fromPriceField.setSelection(fromPriceField.getText().length());
            }
        });
        toPriceField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(toPriceField.getTag() == null) {
                    int val = 0;
                    if(toPriceField.getText().toString().compareTo("") != 0)
                        val = Integer.parseInt(toPriceField.getText().toString());
                    if (val < fromPriceValue) {
                        val = fromPriceValue;
                        toPriceField.setTag("");
                        toPriceField.setText("" + toPriceValue, TextView.BufferType.EDITABLE);
                        toPriceField.setTag(null);
                    }
                    if (val < 0) {
                        val = 0;
                        fromPriceField.setTag("");
                        fromPriceField.setText("" + val, TextView.BufferType.EDITABLE);
                        fromPriceField.setTag(null);
                    }
                    toPriceValue = val;
                    toPriceBar.setProgress(val);
                }
                // Set cursor to the end of the field
                toPriceField.setSelection(toPriceField.getText().length());
            }
        });
        final CloudListViewAdapter langAdapter = new CloudListViewAdapter(ToursFilterActivity.this, langList);
        final CloudListRecyclerViewAdapter langRecyclerAdapter = new CloudListRecyclerViewAdapter(langList, langSelectButton);
        langSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View mView = getLayoutInflater().inflate(R.layout.choose_languages, null);
                ListView langListView;
                // Language selection button
                Button selectButton;
                langListView = mView.findViewById(R.id.languagesList);
                selectButton = mView.findViewById(R.id.selectionButton);
                langListView.setAdapter(langAdapter);
                final WrappedDialog d = new WrappedDialog();
                selectButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        langRecyclerAdapter.buildActualList();
                        // Hide select button
                        if(langRecyclerAdapter.getItemCount() > 0)
                            langSelectButton.setVisibility(View.GONE);
                        // Refresh view
                        langRecyclerAdapter.notifyDataSetChanged();
                        d.dismiss();
                    }
                });
                d.setView(mView);
                d.show(getFragmentManager(), null);
            }
        });

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        mLangRecycler.setLayoutManager(layoutManager);
        mLangRecycler.setAdapter(langRecyclerAdapter);
        // Make selection button visible if no languages are chosen
        if(langList.size() == 0) {
            langSelectButton.setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerToggle.syncState();
    }
    public void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // Expanded state height
        final int targetHeight = v.getMeasuredHeight();
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        // Button rotation angle
        final float ANGLE = 180;

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                interpolatedTime *= 4;
                if (interpolatedTime > 1)
                    interpolatedTime = 1;
                toggleFiltersButton.setRotation(ANGLE * interpolatedTime - 180);
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        a.setDuration((int)(4 * targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        // Button rotation angle
        final float ANGLE = -180;

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                interpolatedTime *= 4;
                if (interpolatedTime > 1)
                    interpolatedTime = 1;
                toggleFiltersButton.setRotation(ANGLE * interpolatedTime);
                if(interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        a.setDuration((int)(4 * initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
