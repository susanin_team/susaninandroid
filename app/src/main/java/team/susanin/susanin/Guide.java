package team.susanin.susanin;

import android.graphics.Bitmap;

public class Guide {
    private String name;
    private String surname;
    private float rating;
    private Bitmap avatar;
    public Guide(String guideName, String guideSurname, float guideRating) {
        name = guideName;
        surname = guideSurname;
        rating = guideRating;
    }
    public Guide(String guideName, String guideSurname, float guideRating, Bitmap guideAvatar) {
        name = guideName;
        surname = guideSurname;
        rating = guideRating;
        avatar = guideAvatar;
    }
    public String getName() {
        return name;
    }
    public void setName(String guideName) {
        name = guideName;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String guideSurname) {
        surname = guideSurname;
    }
    public float getRating() {
        return rating;
    }
    public void setRating(float guideRating) {
        rating = guideRating;
    }
    public void setAvatar(Bitmap guideAvatar) {
        avatar = guideAvatar;
    }
    public Bitmap getAvatar() {
        return avatar;
    }
}
