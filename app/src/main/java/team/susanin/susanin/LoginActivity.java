package team.susanin.susanin;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import team.susanin.susanin.Requests.PostRequest;

public class LoginActivity extends AppCompatActivity {
    private  Button registerBtn, loginBtn, recoveryBtn;
    EditText emailText;
    EditText passwordText;
    // Exceptions of view
    TextView emailEmptyException;
    TextView passwordEmptyException;

    public class CallbackClass implements PostRequest.Callback<JSONObject, VolleyError> {
        @Override
        public void onEventCompleted(JSONObject responce) {
            System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
            try {
                System.out.println(responce.getString("key"));
                SharedPrefManager.getInstance(getApplicationContext())
                        .userLogin(responce.getString("key"));
            }
            catch (JSONException e) {

            }
            startActivity(new Intent(getApplicationContext(), MainScreenActivity.class));
            finish();
        }
        @Override
        public void onEventFailed(VolleyError error) {
            TextView serverException = findViewById(R.id.exceptionServer);
            if(error.networkResponse != null && error.networkResponse.data != null)
                serverException.setText(error.networkResponse.statusCode);
            else
                serverException.setText("Server Error");
            serverException.setVisibility(View.VISIBLE);
        }
    }

    CallbackClass callbackClass = new CallbackClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainScreenActivity.class));
            return;
        }

        emailText = findViewById(R.id.emailText);
        passwordText = findViewById(R.id.passwordText);
        // Exceptions of view
        emailEmptyException = findViewById(R.id.exceptionEmailEmptyText);
        passwordEmptyException = findViewById(R.id.exсeptionPasswordEmptyText);
        registerBtn = findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            StringValidator stringValidator = new StringValidator();
            @Override
            public void onClick(View view) {
                PostRequest postRequest = new PostRequest();
                if (checkFields(stringValidator)){
                    postRequest.registerCallBack(callbackClass);
                    postRequest.post(Constants.LOGIN_REQUEST_URL, loginFormatDataAsJson(emailText.getText().toString(),
                            passwordText.getText().toString()), getApplicationContext());
                }
            }
        });
        recoveryBtn=findViewById(R.id.passwordRecovery);
        // Changing text style on typing
        // For email field
        emailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Empty
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String fieldText = emailText.getText().toString();
                if(fieldText.matches(""))
                    emailText.setTypeface(Typeface.DEFAULT);
                else
                    emailText.setTypeface(Typeface.DEFAULT_BOLD);
            }
            @Override
            public void afterTextChanged(Editable editable) {
                // Empty
            }
        });
        // For password field
        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Empty
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String fieldText = passwordText.getText().toString();
                if(fieldText.matches(""))
                    passwordText.setTypeface(Typeface.DEFAULT);
                else
                    passwordText.setTypeface(Typeface.DEFAULT_BOLD);
            }
            @Override
            public void afterTextChanged(Editable editable) {
                // Empty
            }
        });
    }

    public boolean checkFields(StringValidator stringValidator){
        emailEmptyException.setVisibility(View.INVISIBLE);
        passwordEmptyException.setVisibility(View.INVISIBLE);
        if (stringValidator.isFieldNotEmpty(emailText.getText().toString())) {
            if (stringValidator.isFieldNotEmpty(passwordText.getText().toString())) {
                return true;
            } else {
                passwordEmptyException.setVisibility(View.VISIBLE);
                return false;
            }
        }
        else {
            emailEmptyException.setVisibility(View.VISIBLE);
            return false;
        }
    }

    public JSONObject loginFormatDataAsJson(String email, String password ){
        final JSONObject root = new JSONObject();
        try {
            root.put("username", email);
            root.put("password", password);
            return root;
        } catch (JSONException ej){
            Log.d("JWP", "Can't generate JSON");
        }
        return null;
    }
}
