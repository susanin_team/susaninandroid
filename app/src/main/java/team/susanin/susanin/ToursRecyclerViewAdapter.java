package team.susanin.susanin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class ToursRecyclerViewAdapter extends RecyclerView.Adapter<ToursRecyclerViewAdapter.ViewHolder> {
    private ArrayList<TourItem> mToursList;
    private Context mContext;

    public ToursRecyclerViewAdapter(Context context, ArrayList<TourItem> toursList) {
        mToursList = toursList;
        mContext = context;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mainLayout;
        public ViewHolder(LinearLayout v) {
            super(v);
            mainLayout = v;
        }
        public LinearLayout getMainLayout() {
            return mainLayout;
        }
    }
    @Override
    public ToursRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tour_tour, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int i) {
        LinearLayout mainLayout = holder.getMainLayout();
        TourItem tourItem = mToursList.get(i);
        String tourTitle = tourItem.getTitle();
        Float tourRating = tourItem.getRating();
        int tourVisitors = tourItem.getVisitors();
        Boolean isAdult = tourItem.isAdult();
        // If tour is adult-oriented
        if (isAdult)
            tourTitle += " <font color=\"#993434\">(18+)</font>";
        // Set all fields
        TextView tourTitleField = mainLayout.findViewById(R.id.tourTitle);
        TextView tourRatingField = mainLayout.findViewById(R.id.tourRating);
        TextView tourVisitorsField = mainLayout.findViewById(R.id.tourVisitors);
        tourTitleField.setText(Html.fromHtml(tourTitle));
        tourRatingField.setText(String.format ("%.1f", tourRating));
        // Format visitors counter and set
        tourVisitorsField.setText(Formater.roundIntegerCounter(tourVisitors));
    }
    @Override
    public int getItemCount() {
        return mToursList.size();
    }
}
