package team.susanin.susanin;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class ProfileActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        // Toolbar init
        mDrawerLayout = findViewById(R.id.drawerContainer);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // Test
        Bitmap bmLoader;
        Bitmap bmLoader1;
        ProfileCommentView profView = findViewById(R.id.testComment1);
        ProfileCommentView profView1 = findViewById(R.id.testComment2);
        bmLoader = (BitmapFactory.decodeResource(getResources(), R.drawable.car)).copy(Bitmap.Config.ARGB_8888, true);
        bmLoader1 = (BitmapFactory.decodeResource(getResources(), R.drawable.profile_pic)).copy(Bitmap.Config.ARGB_8888, true);
        profView1.setProfilePicture(bmLoader);
        profView.setProfilePicture(bmLoader1);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerToggle.syncState();
    }
}
