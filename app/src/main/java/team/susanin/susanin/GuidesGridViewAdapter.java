package team.susanin.susanin;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

public class GuidesGridViewAdapter extends BaseAdapter{
    Context mContext;
    ArrayList<Guide> mGuidesList;

    public GuidesGridViewAdapter(Context context, ArrayList<Guide> guidesList) {
        mContext = context;
        mGuidesList = guidesList;
    }
    @Override
    public int getCount() {
        return mGuidesList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View guideView;
        Guide currentGuide = mGuidesList.get(i);
        if(view == null) {
            TextView guideNameView;
            TextView guideSurnameView;
            TextView guideRatingView;
            LayoutInflater inflater = LayoutInflater.from(mContext);
            guideView = inflater.inflate(R.layout.item_guide_view, null, false);
            guideNameView = guideView.findViewById(R.id.guideName);
            guideNameView.setText(currentGuide.getName());
            guideSurnameView = guideView.findViewById(R.id.guideSurname);
            guideSurnameView.setText(currentGuide.getSurname());
            guideRatingView = guideView.findViewById(R.id.guideRating);
            guideRatingView.setText(String.format ("%.1f", currentGuide.getRating()));
            // If avatar is set
            if(currentGuide.getAvatar() != null) {
                CircleImageView imView;
                imView = guideView.findViewById(R.id.guidePic);
                // Setting image bitmap
                imView.setImageBitmap(currentGuide.getAvatar());
            }
            Bitmap bmLoader;
            ImageView starRatingView;
            starRatingView = guideView.findViewById(R.id.starImage);
            if(currentGuide.getRating() <= 2) {
                bmLoader = (BitmapFactory.decodeResource(mContext.getResources(), R.drawable.red_rating_star))
                        .copy(Bitmap.Config.ARGB_8888, true);
                starRatingView.setImageBitmap(bmLoader);
            } else if(currentGuide.getRating() == 5) {
                bmLoader = (BitmapFactory.decodeResource(mContext.getResources(), R.drawable.blue_rating_star))
                        .copy(Bitmap.Config.ARGB_8888, true);
                starRatingView.setImageBitmap(bmLoader);
            }
        } else {
            guideView = view;
        }
        return guideView;
    }
}
