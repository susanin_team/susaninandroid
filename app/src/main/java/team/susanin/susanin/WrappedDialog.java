package team.susanin.susanin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

public class WrappedDialog extends DialogFragment {
    private View mView;

    public void setView(View view) {
        mView = view;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create the dialog
        AlertDialog.Builder db = new AlertDialog.Builder(getActivity());
        AlertDialog dialog;
        if(mView != null)
            db.setView(mView);
        dialog = db.create();
        // Set background color to transparent
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }
}
