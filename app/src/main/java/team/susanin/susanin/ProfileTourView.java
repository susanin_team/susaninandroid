package team.susanin.susanin;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProfileTourView extends LinearLayout {
    private String tourTitle;
    private double tourRating;
    private int tourVisitors;
    private boolean isAdult;
    private TextView tourTitleField;
    private TextView tourRatingField;
    private TextView tourVisitorsField;

    public ProfileTourView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public ProfileTourView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public ProfileTourView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        init(context, attrs, defStyleAttr);
    }
    protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ProfileTourView, defStyleAttr, 0);
        try{
            tourTitle = array.getString(R.styleable.ProfileTourView_tourTitle);
            tourRating = array.getFloat(R.styleable.ProfileTourView_tourRating, -1);
            tourVisitors = array.getInteger(R.styleable.ProfileTourView_tourVisitors, -1);
            isAdult = array.getBoolean(R.styleable.ProfileTourView_isAdult, false);
            // If tour is adult-oriented
            if (isAdult)
                tourTitle += " <font color=\"#993434\">(18+)</font>";
            // Inflating XML layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutInflater.inflate(R.layout.item_tour_tour, this, true);
            // Set all fields
            tourTitleField = findViewById(R.id.tourTitle);
            tourRatingField = findViewById(R.id.tourRating);
            tourVisitorsField = findViewById(R.id.tourVisitors);
            tourTitleField.setText(Html.fromHtml(tourTitle));
            tourRatingField.setText(String.format ("%.1f", tourRating));
            // Format visitors counter and set
            tourVisitorsField.setText(Formater.roundIntegerCounter(tourVisitors));
        } catch(Exception e) {
            // Catching exception
        } finally {
            array.recycle();
        }
    }
    public boolean onTouchEvent(MotionEvent event) {
        int eventAction = event.getAction();
        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                tourTitleField.setTextColor(Color.GRAY);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                tourTitleField.setTextColor(Color.BLACK);
                break;
        }
        invalidate();
        return true;

    }
}
