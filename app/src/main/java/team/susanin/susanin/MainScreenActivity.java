package team.susanin.susanin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;


import java.util.ArrayList;

public class MainScreenActivity extends AppCompatActivity implements OnMapReadyCallback {
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private MapView mapView;
    private GoogleMap gmap;
    private SlidingUpPanelLayout slidingPanel;
    private ImageView sliderArrowView;
    private TextView locationString;
    private ExpandableHeightGridView guidesGridView;
    private ArrayList<Guide> guidesList;
    private MenuOptionView profile, settings, logout, favourites, bids;
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
//        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
//            finish();
//            startActivity(new Intent(this, LoginActivity.class));
//        }

        View menuView = findViewById(R.id.menuSlider);
        profile = menuView.findViewById(R.id.menu_slider_profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        settings = menuView.findViewById(R.id.menu_slider_settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        logout = menuView.findViewById(R.id.menu_slider_logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefManager.getInstance(getBaseContext()).logout();
                finish();
                startActivity(new Intent(MainScreenActivity.this, LoginActivity.class));

            }
        });
        favourites = menuView.findViewById(R.id.menu_slider_favourites);
        favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        bids = menuView.findViewById(R.id.menu_slider_bids);
        bids.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
            }
        });


        // MAP STAFF
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        mapView = findViewById(R.id.map_view);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        slidingPanel = findViewById(R.id.slidingPanelLayout);
        panelListener();
        sliderArrowView = findViewById(R.id.sliderTailArrow);
        // Toolbar init
        mDrawerLayout = findViewById(R.id.drawerContainer);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        catch (NullPointerException e){
            Log.e("Error", "Nullptr");
        }

        locationString = findViewById(R.id.locationHint);
        locationString.setText(Html.fromHtml("You are located in <b>Moscow</b>"));
        // Test guide list init
        guidesList = new ArrayList<>();
        guidesList.add(new Guide("Name", "Surname", 4.5f));
        guidesList.add(new Guide("Name", "Surname", 4.5f));
        guidesList.add(new Guide("Name", "Surname", 7f));
        guidesList.add(new Guide("Name1", "Surname1", 4.5f));
        guidesList.add(new Guide("Name", "Surname", 4.5f));
        guidesList.add(new Guide("Name", "Surname", 5f));
        guidesList.add(new Guide("Name", "Surname", 1.2f));
        guidesList.add(new Guide("Name", "Surname", 4.5f));
        guidesList.add(new Guide("Name", "Surname", 0));

        guidesGridView = findViewById(R.id.guidesContainer);
        guidesGridView.setExpanded(true);
        guidesGridView.setAdapter(new GuidesGridViewAdapter(this, guidesList));
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerToggle.syncState();
    }

    public void panelListener(){
        slidingPanel.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                // Rotation angle
                final float ANGLE = 180;
                sliderArrowView.setRotation(ANGLE * slideOffset);
            }
            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                // Empty method
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng moscow = new LatLng(55.750696, 37.620609);
        gmap = googleMap;
        gmap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });
        MarkerOptions marker = new MarkerOptions()
                                            .position(moscow)
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.susanin_point_marker));
        gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(moscow,12.0f));
        gmap.addMarker(marker);
    }

    
}
