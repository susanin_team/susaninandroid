package team.susanin.susanin;

public class Formater {
    public static String roundIntegerCounter(int counter) {
        String result = "";
        if (counter >= 1000) {
            if (counter >= 1000000) {
                // A suffix for millions
                result += String.format("%.1f", ((double) counter) / 1000000) + "M";
            } else {
                // A suffix for thousands
                result += (counter / 1000) + "K";
            }
        } else {
            result += counter;
        }
        return result;
    }
}
