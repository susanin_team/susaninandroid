package team.susanin.susanin;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class CloudListRecyclerViewAdapter extends RecyclerView.Adapter<CloudListRecyclerViewAdapter.ViewHolder> {
    private ArrayList<CloudListItem> mLangsList;
    private ArrayList<CloudListItem> actualLangsList;
    private Button mSelectButton;

    public CloudListRecyclerViewAdapter(ArrayList<CloudListItem> langsList, Button selectButton) {
        mLangsList = langsList;
        mSelectButton = selectButton;
        buildActualList();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout mainLayout;
        public ViewHolder(ConstraintLayout v) {
            super(v);
            mainLayout = v;
        }
        public void setLangName(String nameString) {
            TextView langTextView;
            langTextView = mainLayout.findViewById(R.id.langName);
            langTextView.setText(nameString);
        }
        public ImageView getDeleteButtonView() {
            return mainLayout.findViewById(R.id.delete);
        }
    }
    @Override
    public CloudListRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_language_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int i) {
        // Getting delete button view
        ImageView deleteButton = holder.getDeleteButtonView();
        holder.setLangName(actualLangsList.get(i).getLangName());
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Unchecking item
                actualLangsList.get(i).setChecked(false);
                // Removing item from the list
                actualLangsList.remove(i);
                notifyDataSetChanged();
                // If no items are selected, show the button
                if(getItemCount() == 0) {
                    mSelectButton.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return actualLangsList.size();
    }
    public void buildActualList() {
        actualLangsList = new ArrayList<>();
        for (CloudListItem item : mLangsList) {
            if(item.isChecked())
                actualLangsList.add(item);
        }
    }
}
