package team.susanin.susanin.Requests;

/**
 * Created by AMamonov2 on 5/22/2019.
 */

public class BaseRequest {

    public boolean sended = false;

    public interface Callback<T, I>{
        void onEventCompleted(T responce);
        void onEventFailed(I error);
    }
    Callback callback;

    public void registerCallBack(Callback callback){
        this.callback = callback;
    }
}

