package team.susanin.susanin.Requests;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONObject;


/**
 * Created by AMamonov on 5/11/2018.
 */

public class PostRequest extends BaseRequest  {

    public void post(final String porsUrl, JSONObject jsonObject, Context context) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.POST, porsUrl, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onEventCompleted(response);
                        sended = true;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onEventFailed(error);
                    }
                }
        ){
            //here I want to post data to sever
        };
        requestQueue.add(jsonobj);

    };

}
