package team.susanin.susanin.Requests;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONObject;


/**
 * Created by AMamonov on 5/11/2018.
 */

public class DeleteRequest extends  BaseRequest{

    public boolean sended = false;


    public void delete (final String porsUrl, JSONObject jsonObject, Context context) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.DELETE, porsUrl, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onEventCompleted(response);
                        sended = true;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onEventFailed(error);
                    }
                }
        );
        requestQueue.add(jsonobj);

    };

}
