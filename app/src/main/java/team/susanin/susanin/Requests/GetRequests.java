package team.susanin.susanin.Requests;

import android.content.Context;
import android.webkit.DownloadListener;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import team.susanin.susanin.StringRequest;

/**
 * Created by AMamonov2 on 10/22/2018.
 */


public class GetRequests extends BaseRequest {

    public void getObject(final  String url, Context context) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onEventCompleted(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onEventFailed(error);
                    }
                }
        );
        requestQueue.add(jsonobj);
    };

    public void getArray(final  String url, Context context) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        final JsonArrayRequest jsonobj = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        callback.onEventCompleted(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onEventFailed(error);
                    }
                }
        );
        requestQueue.add(jsonobj);
    };

}
