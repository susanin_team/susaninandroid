package team.susanin.susanin;

public class CloudListItem {
    private boolean isChecked;
    private String langName;

    public CloudListItem(String name, boolean checkedFlag) {
        langName = name;
        isChecked = checkedFlag;
    }
    public String getLangName() {
        return langName;
    }
    public void setLangName(String langName) {
        this.langName = langName;
    }
    public boolean isChecked() {
        return isChecked;
    }
    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
