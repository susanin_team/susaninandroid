package team.susanin.susanin;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MenuOptionView extends LinearLayout {
    private LinearLayout optionContainer;
    private TextView optionTitleField;
    private TextView updatesNumberField;
    private ImageView optionIconField;
    private int updatesNumber;
    private String optionTitle;
    private int optionImageId;
    private int optionImageIdPressed;
    private OnClickListener listener;

    public MenuOptionView(Context context) {
        super(context);
        init(context, null, 0);
    }
    public MenuOptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public MenuOptionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        init(context, attrs, defStyleAttr);
    }
    protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.MenuOptionView, defStyleAttr, 0);
        try{
            optionTitle = array.getString(R.styleable.MenuOptionView_optionTitle);
            optionImageId = array.getResourceId(R.styleable.MenuOptionView_optionIcon, -1);
            optionImageIdPressed = array.getResourceId(R.styleable.MenuOptionView_optionIconPressed, -1);
            updatesNumber = array.getInteger(R.styleable.MenuOptionView_updatesNumber, -1);
            // Inflating XML layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layoutInflater.inflate(R.layout.menu_option, this, true);
            // Set all fields
            optionTitleField = findViewById(R.id.optionText);
            optionIconField = findViewById(R.id.optionIcon);
            optionContainer = findViewById(R.id.optionContainer);
            updatesNumberField = findViewById(R.id.updatesIcon);
            optionTitleField.setText(optionTitle);
            optionIconField.setImageResource(optionImageId);
            if (updatesNumber > 0) {
                updatesNumberField.setVisibility(View.VISIBLE);
                updatesNumberField.setText(""+ updatesNumber);
            }
        } catch(Exception e) {
            // Catching exception
        } finally {
            array.recycle();
        }
    }
    public boolean onTouchEvent(MotionEvent event) {
        int eventAction = event.getAction();
        switch (eventAction) {
            // On tap
            case MotionEvent.ACTION_DOWN:
                optionContainer.setBackgroundColor(getResources().getColor(R.color.menuItemPressed));
                optionTitleField.setTextColor(getResources().getColor(R.color.colorPrimary));
                optionIconField.setImageResource(optionImageIdPressed);
                updatesNumberField.setBackground(getResources().getDrawable(R.drawable.updates_circle_icon_pressed));
                updatesNumberField.setTextColor(getResources().getColor(R.color.primaryBlue));
                break;
            // On release
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                optionContainer.setBackgroundColor(getResources().getColor(R.color.menuItem));
                optionTitleField.setTextColor(getResources().getColor(R.color.menu));
//                optionIconField.setImageResource(optionImageId);
                updatesNumberField.setBackground(getResources().getDrawable(R.drawable.updates_circle_icon));
                updatesNumberField.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
        }
        invalidate();
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_UP) {
            if(listener != null) listener.onClick(this);
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_UP && (event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER || event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            if(listener != null) listener.onClick(this);
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }
}
