package team.susanin.susanin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.List;

public class CloudListViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<CloudListItem> mCloudList;

    public CloudListViewAdapter(Context context, List<CloudListItem> cloudList) {
        mContext = context;
        mCloudList = cloudList;
    }
    @Override
    public int getCount() {
        return mCloudList.size();
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View v, ViewGroup parent) {
        View view;
        TextView langNameView;
        CloudListItem langItem;
        final CloudListItem tempLangItem = mCloudList.get(i);
        final CheckBox langCheckbox;
        langItem = mCloudList.get(i);
        // If v is already given for reuse
        if(v == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.choose_langs_item, null, false);
        } else {
            view = v;
        }
        langNameView = view.findViewById(R.id.langName);
        langCheckbox = view.findViewById(R.id.langCheck);
        // Checkboxes init
        langNameView.setText(langItem.getLangName());
        langCheckbox.setChecked(langItem.isChecked());
        // onClick listener for a single checkbox
        langCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempLangItem.setChecked(langCheckbox.isChecked());
            }
        });
        return view;
    }
}
